California Pools is one of the largest pool builders in the country and brings award-winning custom pools to the Dallas area. As seen on Pool Kings, we offer expert design solutions and innovative construction techniques to provide our customers with the highest-quality pool or backyard anywhere.

Address: 2578 Southwell Road, Dallas, TX 75229, USA

Phone: 469-726-2776

Website: https://californiapools.com/locations/dallas
